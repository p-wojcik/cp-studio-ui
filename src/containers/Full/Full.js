import React, {Component} from 'react';
import {Switch, Route, Redirect} from 'react-router-dom';
import {Container} from 'reactstrap';
import Header from '../../components/Header/';
import Sidebar from '../../components/Sidebar/';
import Breadcrumb from '../../components/Breadcrumb/';
import Footer from '../../components/Footer/';

import Dashboard from '../../views/Dashboard/';
import CouponsView from '../../views/CouponsView/';
import AddCouponView from '../../views/AddCouponView/';
import SingleCouponView from '../../views/SingleCouponView/SingleCouponView';

class Full extends Component {
  render() {
    return (
      <div className="app">
        <Header />
        <div className="app-body">
          <Sidebar {...this.props}/>
          <main className="main">
            <Breadcrumb />
            <Container fluid>
              <Switch>
                <Route path="/dashboard" name="Dashboard" component={Dashboard}/>
                <Route exact path="/coupons" name="Coupons" component={CouponsView} />
                <Route exact path="/coupons/add" name="New coupon" component={AddCouponView}/>
                <Route path="/coupons/:couponId" name="SingleCoupon" component={SingleCouponView}/>
                <Redirect from="/" to="/dashboard"/>
              </Switch>
            </Container>
          </main>
        </div>
        <Footer />
      </div>
    );
  }
}

export default Full;
