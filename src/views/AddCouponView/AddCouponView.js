import React, { Component } from 'react';
import {Alert, Card, CardBody, CardHeader, CardFooter, Form, FormGroup, FormText, Col, Input, Label, Button} from 'reactstrap';
import {Switch, Redirect} from 'react-router-dom';
import AddCouponViewScopesTable from './AddCouponViewScopesTable';

const axios = require('axios');
const GLOBAL_CONFIG = require('../../../config.json');
const REDIRECT_URI = "/coupons"

class AddCouponView extends Component {

	constructor(props) {
		super(props);
		this.state = {
			coupon : {},
			redirect : false,
			saveError : false
		};
		this.handleAddScope = this.handleAddScope.bind(this);
		this.handleScopeStateToggle = this.handleScopeStateToggle.bind(this);
		this.handleRemoveScopes = this.handleRemoveScopes.bind(this);
		this.storeCouponProperty = this.storeCouponProperty.bind(this);
		this.storeBooleanCouponProperty = this.storeBooleanCouponProperty.bind(this);
		this.saveCoupon = this.saveCoupon.bind(this);
		this.resetForm = this.resetForm.bind(this);
	}

	saveCoupon() {
		const options =
		{
			url		: '/coupons',
			method	: 'POST',
			baseURL	: GLOBAL_CONFIG.couponService.baseUrl,
			headers	: GLOBAL_CONFIG.couponService.requiredHeaders,
			data   	: this.state.coupon
		};

		axios.request(options)
			.then((response) => {
				this.setState({redirect: true});
			})
			.catch((e) => {
				console.error(e);
				this.setState({ saveError : true});
			});
	}

	handleAddScope(scopeId) {
		const copy = Object.assign({}, this.state.coupon);
		if (!copy.scopes) {
			copy.scopes = [];
		}
		copy.scopes.push({
			locationId 	: scopeId,
			active		: true
		});
		this.setState({coupon: copy});
	}

	handleScopeStateToggle(scopeId) {
		const copy = Object.assign({}, this.state.coupon);
		const modifiedScopes = this.state.coupon.scopes.slice();
		const scopeIndex = modifiedScopes.findIndex((s) => s.locationId === scopeId);
		copy.scopes[scopeIndex].active = !copy.scopes[scopeIndex].active;
		this.setState({coupon: copy});
	}

	handleRemoveScopes(selectedRows) {
		const copy = Object.assign({}, this.state.coupon);
		for (const scopeId of selectedRows) {
			const scopeIndex = copy.scopes.findIndex((s) => s.locationId === scopeId);
			copy.scopes.splice(scopeIndex, 1);
		}
		this.setState({coupon: copy});
	}

	storeCouponProperty(event, propertyName) {
		const copy = Object.assign({}, this.state.coupon);
		copy[propertyName] = event.target.value
		this.setState({ coupon : copy });
	}

	storeBooleanCouponProperty(event, propertyName) {
		const copy = Object.assign({}, this.state.coupon);
		copy[propertyName] = event.target.checked
		this.setState({ coupon : copy });
	}

	isCheckboxSelected(propertyName) {
		return !!this.state.coupon[propertyName];
	}

	resetForm() {
		document.getElementById("create-coupon-form").reset();
		this.setState({ coupon : {} });
	}

	render() {
		if (!!this.state.redirect) {
			return (
				<Switch>
					<Redirect to={REDIRECT_URI}/>
				</Switch>
			);
		}
		return (
			<div className="animated fadeIn">
				<Alert color="danger" isOpen={this.state.saveError} toggle={(e) => this.toggleAlert(false)}>
					<strong>Unexpected error occurred while saving coupon. Please contact administrator.</strong>
				</Alert>
				<Card>
					<CardBody>
						<Card>
							<CardHeader>
								New coupon details
							</CardHeader>
							<CardBody>
								<Form className="form-horizontal" id="create-coupon-form">
									<FormGroup row>
										<Col md="3">
											<Label htmlFor="text-input"><b>Name:</b></Label>
										</Col>
										<Col xs="12" md="9">
											<Input type="text" onBlur={(e) => this.storeCouponProperty(e, "name")} />
										</Col>
									</FormGroup>
									<FormGroup row>
										<Col md="3">
											<Label htmlFor="text-input"><b>Coupon secret:</b></Label>
										</Col>
										<Col xs="12" md="9">
											<Input type="text" onBlur={(e) => this.storeCouponProperty(e, "codeSecret")}/>
											<FormText color="muted">Seed necessary for hashing coupon's QR code</FormText>
										</Col>
									</FormGroup>
									<FormGroup row>
										<Col md="3">
											<Label htmlFor="textarea-input"><b>Description:</b></Label>
										</Col>
										<Col xs="12" md="9">
											<Input type="textarea" rows="9"  onBlur={(e) => this.storeCouponProperty(e, "description")} />
										</Col>
									</FormGroup>
									<FormGroup row>
										<Col md="3">
											<Label><b>Active:</b></Label>
										</Col>
										<Col md="9">
											<Input className="form-check-input" type="checkbox"
												checked={this.isCheckboxSelected("active")}
												onChange={(e) => this.storeBooleanCouponProperty(e, "active")} />
										</Col>
									</FormGroup>
									<FormGroup row>
										<Col md="3">
											<Label><b>Media available:</b></Label>

										</Col>
										<Col md="9">
											<Input className="form-check-input" type="checkbox"
												checked={this.isCheckboxSelected("mediaAvailable")}
												onChange={(e) => this.storeBooleanCouponProperty(e, "mediaAvailable")} />
										</Col>
									</FormGroup>
									<AddCouponViewScopesTable coupon={this.state.coupon}
										addScopeHandler={this.handleAddScope}
										removeScopesHandler={this.handleRemoveScopes}
										scopeToggleHandler={this.handleScopeStateToggle}
									/>
								</Form>
							</CardBody>
							<CardFooter>
								<Button type="submit" size="sm" color="primary" onClick={this.saveCoupon}>Add coupon</Button>
								&nbsp;
								<Button size="sm" color="danger" onClick={this.resetForm}>Reset</Button>
							</CardFooter>
						</Card>
					</CardBody>
				</Card>
			</div>
		);
	}
}

export default AddCouponView;