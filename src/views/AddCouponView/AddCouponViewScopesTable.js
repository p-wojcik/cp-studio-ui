import React, { Component } from 'react';
import {Alert, FormGroup, Col, InputGroup, InputGroupAddon, Button, Card, CardBody, CardHeader, Label, Input, Table} from 'reactstrap';
const axios = require('axios');
const GLOBAL_CONFIG = require('../../../config.json');
const _ = require('lodash/core');

class AddCouponViewScopesTable extends Component {

	constructor(props) {
		super(props);
		this.state = {
			selectedRows : [],
			newScopeComponentVisible : false,
			nonUniqueUUID : false,
			scopeId	: null
		};
		this.changeRowSelectionState = this.changeRowSelectionState.bind(this);
		this.selectAllRows = this.selectAllRows.bind(this);
		this.deselectAllRows = this.deselectAllRows.bind(this);
		this.showAddScopeComponent = this.showAddScopeComponent.bind(this);
		this.storeLocationID = this.storeLocationID.bind(this);
		this.removeScopes = this.removeScopes.bind(this);
		this.addScope = this.addScope.bind(this);
		this.toggleAlert = this.toggleAlert.bind(this);
	}

    storeLocationID(event) {
    	this.setState({scopeId : event.target.value});
    }

    addScope(coupon, addScopeHandler) {
    	const scopeId = this.state.scopeId;
    	if (!!coupon && coupon.scopes) {
    		if (coupon.scopes.find((i) => i.locationId === scopeId)) {
    			this.toggleAlert(true);
    		} else {
    			addScopeHandler(scopeId);
    		}
    	} else {
			addScopeHandler(scopeId);
		}
    }

	removeScopes(removeScopesHandler) {
		removeScopesHandler(this.state.selectedRows);
		this.state.selectedRows = [];
	}

	changeRowSelectionState(locationId) {
		const copy = this.state.selectedRows.slice();
		if (this.isRowSelected(locationId)) {
			const index = copy.indexOf(locationId);
			copy.splice(index, 1);
		} else {
			copy.push(locationId);
		}
		this.setState({selectedRows : copy})
	}

	isRowSelected(locationId) {
		return this.state.selectedRows.includes(locationId);
	}

	selectAllRows() {
		const locationIds = this.props.coupon.scopes.map((i) => i.locationId);
		this.setState({selectedRows : locationIds});
	}

	deselectAllRows() {
		this.setState({selectedRows : []});
	}

	showAddScopeComponent() {
		this.setState({newScopeComponentVisible : true});
	}

	toggleAlert(state) {
		this.setState({ nonUniqueUUID : state });
	}

	prepareAddScopeComponent(coupon, addScopeHandler) {
		const cssClass = this.state.newScopeComponentVisible ? "d-block" : "d-none";
		return (
			<FormGroup row className={cssClass}>
				<Col md="12">
				    <InputGroup>
						<Input type="text" id="input2-group2" name="input2-group2" placeholder="Location ID"
							onBlur={this.storeLocationID}/>
						<InputGroupAddon addonType="append">
							<Button type="button" color="primary" onClick={(e) => this.addScope(coupon, addScopeHandler)}>
								Add
							</Button>
						</InputGroupAddon>
				  	</InputGroup>
				</Col>
			</FormGroup>
		);
	}

	prepareTable(coupon, scopeToggleHandler) {
		if(_.isEmpty(coupon) || _.isEmpty(coupon.scopes)) {
			return (<span>No scopes available.</span>);
		}

		let rows = [];
		for (let i=0;i<coupon.scopes.length;i++) {
			const scope = coupon.scopes[i];
			rows.push(
				<tr key={'scopesrow_'+i}>
					<td>{scope.locationId}</td>
					<td>
						<Label className="switch switch-icon switch-primary">
							<Input type="checkbox" className="switch-input" checked={scope.active}
								onChange={(e) => scopeToggleHandler(scope.locationId)}/>
							<span className="switch-label" data-on={'\uf00c'} data-off={'\uf00d'}></span>
							<span className="switch-handle"></span>
						</Label>
					</td>
					<td>
						<Input checked={this.isRowSelected(scope.locationId)}
							className="float-right ml-0" type="checkbox" value={scope.locationId}
							onChange={(e) => this.changeRowSelectionState(scope.locationId)}
						/>
					</td>
				</tr>
			);
		}

		return (
			<Table responsive striped>
				<thead>
					<tr>
						<th>Location</th>
						<th>Status</th>
						<th>Select</th>
					</tr>
				</thead>
				<tbody>
					{rows}
				</tbody>
			</Table>
		);
	}

	render() {
		const { coupon, addScopeHandler, removeScopesHandler, scopeToggleHandler} = this.props;
		const table = this.prepareTable(coupon, scopeToggleHandler);
		const addScopeComponent = this.prepareAddScopeComponent(coupon, addScopeHandler);
		return (<div>
			<Card className="border-primary">
				<CardHeader>
					Scopes
				</CardHeader>
				<CardBody>
					<Alert color="warning" isOpen={this.state.nonUniqueUUID}
						toggle={(e) => this.toggleAlert(false)}>
					  <strong>Scope with given ID already exists. Please choose another one.</strong>
					</Alert>
					<Button color={"primary"} onClick={this.showAddScopeComponent}>
						Add scope
					</Button>
					&nbsp;
					<Button color={"secondary"} onClick={this.selectAllRows}>
						Select all
					</Button>
					&nbsp;
					<Button color={"secondary"} onClick={this.deselectAllRows}>
						Deselect all
					</Button>
					&nbsp;
					<Button disabled={this.state.selectedRows.length===0} color={"danger"}
						onClick={(e) => this.removeScopes(removeScopesHandler)}>
						Remove selected
					</Button>
					<br/><br/>
					{addScopeComponent}
					{table}
				</CardBody>
			</Card>
		</div>);
	}
}

export default AddCouponViewScopesTable;