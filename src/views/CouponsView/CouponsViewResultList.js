import React, { Component } from 'react';
import {Card, CardHeader, CardBody, Row, Col} from 'reactstrap';
import CouponCard from '../../components/CouponCard/CouponCard';
const _ = require('lodash/core');

const COLUMNS_COUNT = 2;

class CouponsViewResultList extends Component {

	prepareCards() {
		const {coupons} = this.props;
    	if (_.isEmpty(coupons)) {
    		return "No coupons available.";
    	}

    	let rows = [];
    	for (let i=0;i<coupons.length;i++) {
    		let cols = [];
    		for (let j=0, index=(i*COLUMNS_COUNT)+j;j<COLUMNS_COUNT && coupons[index];j++, index+=j) {
    			const coupon = coupons[index];
    			const couponCard = this.createCouponCard(index, coupon);
    			cols.push(couponCard);
    		}
    		rows.push(<Row key={'card_row'+i}>{cols}</Row>)
    	}
    	return rows;
    }

    createCouponCard(index, coupon) {
    	const {cardToggleSuccessHandler, cardToggleFailureHandler, scopeToggleSuccessHandler} = this.props;
    	return (
			<Col key={index}>
				<CouponCard card={coupon}
					icon="fa fa-fort-awesome"
					link="/#/coupons"
					cardToggleSuccessHandler={cardToggleSuccessHandler}
					cardToggleFailureHandler={cardToggleFailureHandler}
					scopeToggleSuccessHandler={scopeToggleSuccessHandler}
				/>
			</Col>
    	);
    }

	render() {
		const cards = this.prepareCards();
		return (
			<Card className="border-primary">
				<CardHeader>
					Coupons list
				</CardHeader>
				<CardBody>
					{cards}
				</CardBody>
			</Card>

		);
	}
}

export default CouponsViewResultList;