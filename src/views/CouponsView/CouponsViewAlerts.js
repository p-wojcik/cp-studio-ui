import React, { Component } from 'react';
import {Alert} from 'reactstrap';

class CouponsViewAlerts extends Component{

	render() {
		const {alerts, closeAlert} = this.props;

		return (<div>
			<Alert color="danger" isOpen={alerts.getAllFailure}
				toggle={(e) => closeAlert("getAllFailure")}>
			  <strong>Unexpected error occurred while getting list of coupons. Please contact administrator.</strong>
			</Alert>
			<Alert color="success" isOpen={alerts.enableAllSuccess}
				toggle={(e) => closeAlert("enableAllSuccess")}>
			  <strong>All coupons have been successfully activated!</strong>
			</Alert>
			<Alert color="danger" isOpen={alerts.enableAllFailure}
				toggle={(e) => closeAlert("enableAllFailure")}>
			  <strong>Unexpected error occurred while activating all coupons. Please contact administrator.</strong>
			</Alert>
			<Alert color="success" isOpen={alerts.disableAllSuccess}
				toggle={(e) => closeAlert("disableAllSuccess")}>
			  <strong>All coupons have been successfully inactivated!</strong>
			</Alert>
			<Alert color="danger" isOpen={alerts.disableAllFailure}
				toggle={(e) => closeAlert("disableAllFailure")}>
			  <strong>Unexpected error occurred while inactivating all coupons. Please contact administrator.</strong>
			</Alert>
			<Alert color="danger" isOpen={alerts.enableFailure}
				toggle={(e) => closeAlert("enableFailure")}>
			  <strong>Unexpected error occurred while activating coupon. Please contact administrator.</strong>
			</Alert>
			<Alert color="danger" isOpen={alerts.disableFailure}
				toggle={(e) => closeAlert("disableFailure")}>
			  <strong>Unexpected error occurred while inactivating coupon. Please contact administrator.</strong>
			</Alert>
		</div>);
	}
}

export default CouponsViewAlerts;