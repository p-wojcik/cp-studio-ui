import React, { Component } from 'react';
import {Card, CardHeader, CardBody} from 'reactstrap';
import ButtonWithTooltip from '../../components/ButtonWithTooltip/ButtonWithTooltip';
import {Switch, Redirect} from 'react-router-dom';
const axios = require('axios');
const GLOBAL_CONFIG = require('../../../config.json');
const REDIRECT_URI = 'coupons/add';
class CouponsViewGeneralSettings extends Component {

	constructor(props) {
		super(props);
		this.state = {
			addCouponToRedirect : false
		};
		this.redirectToAddCoupon = this.redirectToAddCoupon.bind(this);
	}

	changeActivityStatusForAllCoupons(enable, enableOrDisableSuccessHandler, enableOrDisableFailureHandler) {
		const httpMethod = enable ? "put" : "delete";
		const options =
		{
			url		: '/coupons/active',
			method	: httpMethod,
			baseURL	: GLOBAL_CONFIG.couponService.baseUrl,
			headers	: GLOBAL_CONFIG.couponService.requiredHeaders
		};

		axios.request(options)
			.then((r) => enableOrDisableSuccessHandler(enable))
			.catch((e) => {
				console.error(e);
				enableOrDisableFailureHandler(enable);
			});
    }

    redirectToAddCoupon() {
    	this.setState({ addCouponToRedirect : true });
    }

    prepareLayout() {
    	if (!!this.state.addCouponToRedirect) {
    		return (
    			<Switch>
					<Redirect to={REDIRECT_URI}/>
				</Switch>
			);
    	}

		const {
			enableOrDisableSuccessHandler,
			enableOrDisableFailureHandler,
			noCoupons
		} = this.props;

		return (
 			<Card className="border-primary">
 				<CardHeader>
 					General settings
 			  	</CardHeader>
 			  	<CardBody>
 					<ButtonWithTooltip color="primary" id="button-add" icon="fa fa-plus" tooltipText="Add new coupon"
 						onClick={this.redirectToAddCoupon} />
 					&nbsp;
 					<ButtonWithTooltip disabled={noCoupons} color="secondary" id="button-enable-all" icon="fa fa-unlock"
 						tooltipText="Activate all coupons"
 						onClick={(e) => this.changeActivityStatusForAllCoupons(true, enableOrDisableSuccessHandler,
 						enableOrDisableFailureHandler)} />
 					&nbsp;
 					<ButtonWithTooltip disabled={noCoupons} color="secondary" id="button-disable-all" icon="fa fa-lock"
 						tooltipText="Inactivate all coupons"
 						onClick={(e) => this.changeActivityStatusForAllCoupons(false, enableOrDisableSuccessHandler,
 						enableOrDisableFailureHandler)} />
 			  </CardBody>
 			</Card>
 		);

    }

	render() {
		const layout = this.prepareLayout();
		return (
			<div>
				{layout}
			</div>
		);
	}
}

export default CouponsViewGeneralSettings;