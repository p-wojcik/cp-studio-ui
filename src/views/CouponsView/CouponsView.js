import React, { Component } from 'react';
import {Card, CardBody} from 'reactstrap';
import CouponsViewGeneralSettings from './CouponsViewGeneralSettings';
import CouponsViewAlerts from './CouponsViewAlerts';
import CouponsViewResultList from './CouponsViewResultList';

const GLOBAL_CONFIG = require('../../../config.json');
const axios = require('axios');
const _ = require('lodash/core');

class CouponsView extends Component {

  constructor(props) {
  	super(props);
  	this.state = {
  		coupons : [],
  		alerts : {
  			getAllFailure	 : false,
  			enableAllSuccess : false,
  			enableAllFailure : false,
  			disableAllSuccess : false,
  			disableAllFailure : false,
  			enableFailure : false,
  			disableFailure : false
  		}
  	};
  	this.handleSuccessGet = this.handleSuccessGet.bind(this);
  	this.handleFailureGet = this.handleFailureGet.bind(this);
  	this.handleSuccessCardToggle = this.handleSuccessCardToggle.bind(this);
  	this.handleSuccessScopeToggle = this.handleSuccessScopeToggle.bind(this);
  	this.handleAllCouponsStateChange = this.handleAllCouponsStateChange.bind(this);
  	this.handleAllCouponsStateChangeFailure = this.handleAllCouponsStateChangeFailure.bind(this);
  	this.handleSingleCouponStateChangeFailure = this.handleSingleCouponStateChangeFailure.bind(this);
  	this.openAlert = this.openAlert.bind(this);
  	this.closeAlert = this.closeAlert.bind(this);
  }

  componentWillMount() {
  	const options =
	{
		baseURL: GLOBAL_CONFIG.couponService.baseUrl,
		headers: GLOBAL_CONFIG.couponService.requiredHeaders
	};

	axios.get('/coupons', options)
		.then(this.handleSuccessGet)
		.catch((e) => {
			console.error(e);
			this.openAlert("getAllFailure");
		});
  }

  handleSuccessGet(response) {
  	this.setState({coupons: response.data});
  }

  handleFailureGet(error) {
  	console.error(error);
    this.setState({coupons: []});
  }

  handleSuccessCardToggle(couponId) {
  	const freshCopy = this.state.coupons.slice();
  	const index = freshCopy.findIndex((c) => c.id == couponId);
  	freshCopy[index].active = !freshCopy[index].active;
  	this.setState({coupons: freshCopy});
  }

  handleSuccessScopeToggle(couponId, scopeId) {
  	  const freshCopy = this.state.coupons.slice();
  	  const couponIndex = freshCopy.findIndex((c) => c.id == couponId);
  	  const modifiedScopes = freshCopy[couponIndex].scopes;
  	  const scopeIndex = modifiedScopes.findIndex((s) => s.locationId == scopeId);
  	  freshCopy[couponIndex].scopes[scopeIndex].active = !freshCopy[couponIndex].scopes[scopeIndex].active;
  	  this.setState({coupons: freshCopy});
  }

  handleAllCouponsStateChange(state) {
  	const freshCopy = this.state.coupons.slice();
  	_.forEach(freshCopy, (coupon) => {
  		coupon.active = state;
  	});

  	const propertyNameToChange = state ? "enableAllSuccess" : "disableAllSuccess";
  	const alertsMap = this.getChangedAlertMap(propertyNameToChange, true);
  	this.setState({
  		coupons: freshCopy,
  		alerts : alertsMap
  	});
  }

  handleAllCouponsStateChangeFailure(state) {
  	const propertyNameToChange = state ? "enableAllFailure" : "disableAllFailure";
  	this.openAlert(propertyNameToChange);
  }

  handleSingleCouponStateChangeFailure(state) {
	const propertyNameToChange = state ? "disableFailure" : "enableFailure";
	this.openAlert(propertyNameToChange);
}

  openAlert(propertyName) {
  	const alertsMap = this.getChangedAlertMap(propertyName, true);
	this.setState({
		alerts : alertsMap
	});
  }

  closeAlert(propertyName) {
  	const alertsMap = this.getChangedAlertMap(propertyName, false);
	this.setState({
		alerts : alertsMap
	});
  }

  getChangedAlertMap(propertyNameToChange, newState) {
  	const alertsMap = Object.assign({}, this.state.alerts);
	alertsMap[propertyNameToChange] = newState;
	return alertsMap;
  }

  render() {
    return (
		<div className="animated fadeIn">
			<CouponsViewAlerts alerts={this.state.alerts} closeAlert={this.closeAlert} />
			<Card>
				<CardBody>
					<CouponsViewGeneralSettings
						noCoupons={_.isEmpty(this.state.coupons)}
						enableOrDisableSuccessHandler={this.handleAllCouponsStateChange}
						enableOrDisableFailureHandler={this.handleAllCouponsStateChangeFailure}
					/>
					<CouponsViewResultList
						coupons={this.state.coupons}
						cardToggleSuccessHandler={this.handleSuccessCardToggle}
						cardToggleFailureHandler={this.handleSingleCouponStateChangeFailure}
						scopeToggleSuccessHandler={this.handleSuccessScopeToggle}
					/>
				</CardBody>
			</Card>
		</div>
    );
  }
}

export default CouponsView;
