import React, { Component } from 'react';
import {Card, CardHeader, CardBody, Row, Col} from 'reactstrap';

class SingleCouponViewGeneralInfo extends Component {

	render() {
		const coupon = this.props.coupon;

		return (
			<Card className="border-primary">
				<CardHeader>
					General
				</CardHeader>
				<CardBody>
					<Row className="mb-2">
						<Col xs="2"><b>Identifier:</b></Col>
						<Col>{coupon.id}</Col>
					</Row>
					<Row className="mb-2">
						<Col xs="2"><b>Code:</b></Col>
						<Col>{coupon.code}</Col>
					</Row>
					<Row>
						<Col xs="2"><b>Description:</b></Col>
						<Col><i>{coupon.description}</i></Col>
					</Row>
				</CardBody>
			</Card>
		);
	}
}

export default SingleCouponViewGeneralInfo;