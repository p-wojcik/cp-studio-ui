import React, { Component } from 'react';
import {FormGroup, Col, InputGroup, InputGroupAddon, Button, Card, CardBody, CardHeader, Label, Input, Table} from 'reactstrap';
import SingleCouponViewScopesTableAlerts from './SingleCouponViewScopesTableAlerts';
const axios = require('axios');
const GLOBAL_CONFIG = require('../../../config.json');
const _ = require('lodash/core');

class SingleCouponViewScopesTable extends Component {

	constructor(props) {
		super(props);
		this.state = {
			selectedRows : [],
			newScopeComponentVisible : false,
			scopeId	: null,
			alerts : {
				nonUniqueUUID	: false,
				addFailure		: false,
				enableSuccess	: false,
				enableFailure	: false,
				disableSuccess	: false,
				disableFailure	: false,
				removeSuccess	: false,
				removeFailure	: false
			}
		};
		this.changeRowSelectionState = this.changeRowSelectionState.bind(this);
		this.openAlert = this.openAlert.bind(this);
		this.closeAlert = this.closeAlert.bind(this);
		this.selectAllRows = this.selectAllRows.bind(this);
		this.deselectAllRows = this.deselectAllRows.bind(this);
		this.showAddScopeComponent = this.showAddScopeComponent.bind(this);
		this.storeLocationID = this.storeLocationID.bind(this);
	}

    storeLocationID(event) {
    	this.setState({scopeId : event.target.value});
    }

    addScope(couponId, successHandler) {
    	if (this.props.coupon.scopes && this.props.coupon.scopes.find((i) => i.locationId === this.state.scopeId)) {
    		this.openAlert("nonUniqueUUID");
    	}
    	else {
    		const scopeToAdd =
				{
					"locationId" : this.state.scopeId,
					"active" : true
				};
				const options =
				{
					url		: '/coupons/' + couponId + '/scopes',
					method	: 'patch',
					data	: [scopeToAdd],
					baseURL	: GLOBAL_CONFIG.couponService.baseUrl,
					headers	: GLOBAL_CONFIG.couponService.requiredHeaders
				};

				axios.request(options)
					.then((e) => {
						successHandler(scopeToAdd);
					})
					.catch((e) => {
						console.error(e);
						this.openAlert("addFailure");
					});
    	}
    }

	onScopeToggle(couponId, scope, successHandler) {
      	const httpMethod = scope.active ? 'delete' : 'put';
		const options =
		{
			url		: '/coupons/' + couponId + '/active/location/' + scope.locationId,
			method	: httpMethod,
			baseURL	: GLOBAL_CONFIG.couponService.baseUrl,
			headers	: GLOBAL_CONFIG.couponService.requiredHeaders
		};

		axios.request(options)
			.then((r) => {
				if (scope.active) {
					this.openAlert("disableSuccess");
				} else {
					this.openAlert("enableSuccess");
				}
				successHandler(scope.locationId);
			}).catch((e) => {
				console.error(e);
				if (scope.active) {
					this.openAlert("disableFailure");
				} else {
					this.openAlert("enableFailure");
				}
			});
    }

	removeScopes(couponId, successHandler) {
		const options =
		{
			url		: '/coupons/' + couponId + '/scopes',
			method	: 'delete',
			data	: this.state.selectedRows,
			baseURL	: GLOBAL_CONFIG.couponService.baseUrl,
			headers	: GLOBAL_CONFIG.couponService.requiredHeaders
		};

		axios.request(options)
			.then((r) => {
				const rows = this.state.selectedRows.slice();
				this.openAlert("removeSuccess");
				this.deselectAllRows();
				successHandler(rows);
			})
			.catch((e) => {
				this.openAlert("removeFailure");
				console.error(e);
			});
	}

	changeRowSelectionState(locationId) {
		const copy = this.state.selectedRows.slice();
		if (this.isRowSelected(locationId)) {
			const index = copy.indexOf(locationId);
			copy.splice(index, 1);
		} else {
			copy.push(locationId);
		}
		this.setState({selectedRows : copy})
	}

	isRowSelected(locationId) {
		return this.state.selectedRows.includes(locationId);
	}

	selectAllRows() {
		const locationIds = this.props.coupon.scopes.map((i) => i.locationId);
		this.setState({selectedRows : locationIds});
	}

	deselectAllRows() {
		this.setState({selectedRows : []});
	}

	openAlert(propertyName) {
		const alertsMap = this.getChangedAlertMap(propertyName, true);
		this.setState({
			alerts : alertsMap
		});
	}

	closeAlert(propertyName) {
		const alertsMap = this.getChangedAlertMap(propertyName, false);
		this.setState({
			alerts : alertsMap
		});
	}

	getChangedAlertMap(propertyNameToChange, newState) {
		const alertsMap = Object.assign({}, this.state.alerts);
		alertsMap[propertyNameToChange] = newState;
		return alertsMap;
	}

	showAddScopeComponent() {
		this.setState({newScopeComponentVisible : true});
	}

	prepareAddScopeComponent(couponId, addScopeSuccessHandler) {
		const cssClass = this.state.newScopeComponentVisible ? "d-block" : "d-none";
		return (
			<FormGroup row className={cssClass}>
				<Col md="12">
				  <InputGroup>
					<Input type="text" id="input2-group2" name="input2-group2" placeholder="Location ID"
						onBlur={this.storeLocationID}/>
					<InputGroupAddon addonType="append">
					  <Button type="button" color="primary"
					  	onClick={(e) => this.addScope(couponId, addScopeSuccessHandler)}>
					  	Add
					  </Button>
					</InputGroupAddon>
				  </InputGroup>
				</Col>
			</FormGroup>
		);
	}

	prepareTable(coupon, toggleScopeSuccessHandler) {
		if(_.isEmpty(coupon.scopes)) {
			return (<span>No scopes available.</span>);
		}

		let rows = [];
		for (let i=0;i<coupon.scopes.length;i++) {
			const scope = coupon.scopes[i];
			rows.push(
				<tr key={'scopesrow_'+i}>
					<td>{scope.locationId}</td>
					<td>
						<Label className="switch switch-icon switch-primary">
							<Input type="checkbox" className="switch-input" checked={scope.active}
								onChange={(e) => {
									this.onScopeToggle(coupon.id, scope, toggleScopeSuccessHandler)}}/>
							<span className="switch-label" data-on={'\uf00c'} data-off={'\uf00d'}></span>
							<span className="switch-handle"></span>
						</Label>
					</td>
					<td>
						<Input checked={this.isRowSelected(scope.locationId)}
							className="float-right ml-0" type="checkbox" value={scope.locationId}
							onChange={(e) => this.changeRowSelectionState(scope.locationId)}
						/>
					</td>
				</tr>
			);
		}

		return (
			<Table responsive striped>
				<thead>
					<tr>
						<th>Location</th>
						<th>Status</th>
						<th>Select</th>
					</tr>
				</thead>
				<tbody>
					{rows}
				</tbody>
			</Table>
		);
	}

	render() {
		const {coupon, addScopeSuccessHandler, toggleScopeSuccessHandler, removeScopesSuccessHandler} = this.props;
		const table = this.prepareTable(coupon, toggleScopeSuccessHandler);
		const addScopeComponent = this.prepareAddScopeComponent(coupon.id,addScopeSuccessHandler);
		return (<div>
			<SingleCouponViewScopesTableAlerts alerts={this.state.alerts} closeAlert={this.closeAlert} />
			<Card className="border-primary">
				<CardHeader>
					Scopes
				</CardHeader>
				<CardBody>
					<Button color={"primary"} onClick={this.showAddScopeComponent}>
						Add scope
					</Button>
					&nbsp;
					<Button color={"secondary"} onClick={this.selectAllRows}>
						Select all
					</Button>
					&nbsp;
					<Button color={"secondary"} onClick={this.deselectAllRows}>
						Deselect all
					</Button>
					&nbsp;
					<Button disabled={this.state.selectedRows.length===0} color={"danger"}
						onClick={(e) => this.removeScopes(this.props.coupon.id, removeScopesSuccessHandler)} >
						Remove selected
					</Button>
					<br/><br/>
					{addScopeComponent}
					{table}
				</CardBody>
			</Card>
		</div>);
	}
}

export default SingleCouponViewScopesTable;