import React, { Component } from 'react';
import {Alert, Card, CardBody} from 'reactstrap';
import {Switch, Redirect} from 'react-router-dom';
import SingleCouponDetailsView from './SingleCouponDetailsView';
const axios = require('axios');
const GLOBAL_CONFIG = require('../../../config.json');
const REDIRECT_URI = "/coupons"

class SingleCouponView extends Component {

	constructor(props) {
		super(props);
		this.state = {
			coupon : null,
			redirect : false,
			getFailureAlertOpen : false
		};
		this.openAlert = this.openAlert.bind(this);
		this.closeAlert = this.closeAlert.bind(this);
		this.handleActivityStateChangeSuccess = this.handleActivityStateChangeSuccess.bind(this);
		this.handleRemovalSuccess = this.handleRemovalSuccess.bind(this);
		this.handleSuccessScopeToggle = this.handleSuccessScopeToggle.bind(this);
		this.handleSuccessScopeAdd = this.handleSuccessScopeAdd.bind(this);
		this.handleSuccessScopesRemoval = this.handleSuccessScopesRemoval.bind(this);
	}

	componentWillMount() {
		const couponId = this.props.match.params.couponId;
		const options =
		{
			baseURL: GLOBAL_CONFIG.couponService.baseUrl,
			headers: GLOBAL_CONFIG.couponService.requiredHeaders
		};

		axios.get('/coupons/' + couponId, options)
			.then((response) => {
				this.setState({coupon: response.data});
			})
			.catch((e) => {
				console.error(e);
				this.openAlert();
			});
	}

	handleActivityStateChangeSuccess(coupon) {
		var copy = Object.assign({}, coupon, {active: !coupon.active});
		this.setState({ coupon : copy})
	}

	handleRemovalSuccess() {
		this.setState({redirect : true})
	}

	handleSuccessScopeToggle(scopeId) {
		const copy = Object.assign({}, this.state.coupon);
		const modifiedScopes = this.state.coupon.scopes.slice();
		const scopeIndex = modifiedScopes.findIndex((s) => s.locationId == scopeId);
		copy.scopes[scopeIndex].active = !copy.scopes[scopeIndex].active;
		this.setState({coupon: copy});
	}

	handleSuccessScopeAdd(scope) {
		const copy = Object.assign({}, this.state.coupon);
		copy.scopes.push(scope);
		this.setState({coupon: copy});
	}

	handleSuccessScopesRemoval(scopeIds) {
		const copy = Object.assign({}, this.state.coupon);
		for (const scopeId of scopeIds) {
			const scopeIndex = copy.scopes.findIndex((s) => s.locationId == scopeId);
			copy.scopes.splice(scopeIndex, 1);
		}
		this.setState({coupon: copy});
	}

	openAlert() {
		this.setState({getFailureAlertOpen : true});
	}

	closeAlert() {
		this.setState({getFailureAlertOpen : false});
	}

	prepareLayout() {
		if (!!this.state.redirect) {
			return (
				<Switch>
					<Redirect to={REDIRECT_URI}/>
				</Switch>
			);
		} else if (this.state.coupon) {
			return (
				<SingleCouponDetailsView coupon={this.state.coupon}
					removalSuccessHandler={this.handleRemovalSuccess}
					activityStateChangeSuccessHandler={this.handleActivityStateChangeSuccess}
					addScopeSuccessHandler={this.handleSuccessScopeAdd}
					removeScopesSuccessHandler={this.handleSuccessScopesRemoval}
					toggleScopeSuccessHandler={this.handleSuccessScopeToggle} />
			);
        } else {
			return (
				<Card>
					<CardBody>
						Invalid state, please reload page.
					</CardBody>
				</Card>
			);
		}
	}

	render() {
		const layout = this.prepareLayout();
		return (
			<div>
				<Alert color="danger" isOpen={this.state.getFailureAlertOpen} toggle={this.closeAlert}>
				  <strong>Unexpected error occurred while getting coupon details. Please contact administrator.</strong>
				</Alert>
		 		{layout}
		 	</div>);
	}

}

export default SingleCouponView;