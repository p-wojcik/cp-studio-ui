import React, { Component } from 'react';
import {Alert} from 'reactstrap';

class SingleCouponViewScopesTableAlerts extends Component {

	render() {
		const {alerts, closeAlert} = this.props;

		return (<div>
			<Alert color="warning" isOpen={alerts.nonUniqueUUID}
				toggle={(e) => closeAlert("nonUniqueUUID")}>
			  <strong>Scope with given ID already exists. Please choose another one.</strong>
			</Alert>
			<Alert color="danger" isOpen={alerts.addFailure}
				toggle={(e) => closeAlert("addFailure")}>
			  <strong>Unexpected error occurred while adding scope. Please contact administrator.</strong>
			</Alert>
			<Alert color="success" isOpen={alerts.enableSuccess}
				toggle={(e) => closeAlert("enableSuccess")}>
			  <strong>Scope has been successfully activated!</strong>
			</Alert>
			<Alert color="danger" isOpen={alerts.enableFailure}
				toggle={(e) => closeAlert("enableFailure")}>
			  <strong>Unexpected error occurred while activating scope. Please contact administrator.</strong>
			</Alert>
			<Alert color="success" isOpen={alerts.disableSuccess}
				toggle={(e) => closeAlert("disableSuccess")}>
			  <strong>Scope has been successfully inactivated!</strong>
			</Alert>
			<Alert color="danger" isOpen={alerts.disableFailure}
				toggle={(e) => closeAlert("disableFailure")}>
			  <strong>Unexpected error occurred while inactivating scope. Please contact administrator.</strong>
			</Alert>
			<Alert color="success" isOpen={alerts.removeSuccess}
				toggle={(e) => closeAlert("removeSuccess")}>
			  <strong>Scopes has been successfully removed!</strong>
			</Alert>
			<Alert color="danger" isOpen={alerts.removeFailure}
				toggle={(e) => closeAlert("removeFailure")}>
			  <strong>Unexpected error occurred while removing scopes. Please contact administrator.</strong>
			</Alert>
		</div>);
	}
}

export default SingleCouponViewScopesTableAlerts;