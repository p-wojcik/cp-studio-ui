import React, { Component } from 'react';
import {Col, Row, Card, CardBody, CardFooter, Label, Input} from 'reactstrap';
import ButtonWithTooltip from '../../components/ButtonWithTooltip/ButtonWithTooltip';
import classNames from 'classnames';
const axios = require('axios');
const GLOBAL_CONFIG = require('../../../config.json');

class SingleCouponViewHeaderPanel extends Component {

	changeActivityStatusForCoupon(coupon, successHandler, failureHandler) {
		const httpMethod = coupon.active ? "delete" : "put" ;
		const options =
		{
			url		: '/coupons/' + coupon.id + '/active',
			method	: httpMethod,
			baseURL	: GLOBAL_CONFIG.couponService.baseUrl,
			headers	: GLOBAL_CONFIG.couponService.requiredHeaders
		};

		axios.request(options)
			.then((r) => successHandler(coupon))
			.catch((e) => {
				console.error(e);
				const alertToOpen = coupon.active ? "disableFailure" : "enableFailure";
				failureHandler(alertToOpen);
			});
	}

	removeCoupon(coupon, successHandler, failureHandler) {
		const options =
		{
			url		: '/coupons/' + coupon.id,
			method	: 'delete',
			baseURL	: GLOBAL_CONFIG.couponService.baseUrl,
			headers	: GLOBAL_CONFIG.couponService.requiredHeaders
		};

		axios.request(options)
			.then(successHandler)
			.catch((e) => {
				console.error(e);
				failureHandler();
			});
	}

	blockIcon() {
	  const classes = classNames("p-3", 'bg-primary', "fa fa-question", "font-2xl mr-3 float-left");
	  return (<i className={classes}></i>);
	};

	prepareImage(coupon) {
		if (coupon.mediaAvailable) {
			return (
				<div className="avatar">
					<img src="http://media1.santabanta.com/full1/Creative/Abstract/abstract-450a.jpg"
						className="align-bottom img-avatar rounded float-left" alt="Coupon Logo" />
				</div>);
		}
		return this.blockIcon();
	}

	render() {
		const {
			coupon,
			activityStateChangeSuccessHandler,
			activityStateChangeFailureHandler,
			removalSuccessHandler,
			removeFailureHandler
		} = this.props;

		const image = this.prepareImage(coupon);

		return (
			<Card className="border-primary">
				<CardBody>
					<Row>
						<Col xs="1" className="align-top">
							{image}
						</Col>
						<Col className="align-bottom">
							<span className="align-top display-4">{coupon.name}</span>
						</Col>
					</Row>
				</CardBody>
				<CardFooter>
					<Row>
						<Col>
							<Label className="mt-3 float-left switch switch-icon switch-primary">
								<Input type="checkbox" className="switch-input" checked={coupon.active}
									onChange={(e) => this.changeActivityStatusForCoupon(coupon,
									 activityStateChangeSuccessHandler, activityStateChangeFailureHandler) }/>
								<span className="switch-label" data-on={'\uf00c'} data-off={'\uf00d'}></span>
								<span className="switch-handle"></span>
							</Label>
						</Col>
						<Col>
							<ButtonWithTooltip color="danger" id="button-remove" icon="fa fa-trash"
								tooltipText="Remove coupon" cssClass="float-right"
								onClick={(e) => this.removeCoupon(coupon, removalSuccessHandler, removeFailureHandler) } />
						</Col>
					</Row>
				</CardFooter>
			</Card>
		);
	}
}

export default SingleCouponViewHeaderPanel;