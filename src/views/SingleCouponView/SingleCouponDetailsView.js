import React, { Component } from 'react';
import {Card, CardBody} from 'reactstrap';
import SingleCouponViewAlerts from './SingleCouponViewAlerts';
import SingleCouponViewHeaderPanel from './SingleCouponViewHeaderPanel';
import SingleCouponViewGeneralInfo from './SingleCouponViewGeneralInfo';
import SingleCouponViewScopesTable from './SingleCouponViewScopesTable';

class SingleCouponDetailsView extends Component {

	constructor(props) {
		super(props);
		this.state = {
			alerts : {
				enableSuccess : false,
				enableFailure : false,
				disableSuccess : false,
				disableFailure : false,
				removeFailure : false
			}
		};
		this.handleActivityStateChangeSuccess = this.handleActivityStateChangeSuccess.bind(this);
		this.openAlert = this.openAlert.bind(this);
		this.closeAlert = this.closeAlert.bind(this);
	}

	handleActivityStateChangeSuccess(coupon) {
		this.props.activityStateChangeSuccessHandler(coupon);
		const alertToOpen = coupon.active ? "disableSuccess" : "enableSuccess";
		this.openAlert(alertToOpen);
	}

	openAlert(propertyName) {
		const alertsMap = this.getChangedAlertMap(propertyName, true);
		this.setState({
			alerts : alertsMap
		});
	}

	closeAlert(propertyName) {
		const alertsMap = this.getChangedAlertMap(propertyName, false);
		this.setState({
			alerts : alertsMap
		});
	}

	getChangedAlertMap(propertyNameToChange, newState) {
		const alertsMap = Object.assign({}, this.state.alerts);
		alertsMap[propertyNameToChange] = newState;
		return alertsMap;
	}

	render() {
		const {coupon, addScopeSuccessHandler, toggleScopeSuccessHandler, removeScopesSuccessHandler} = this.props;
		return (
			<div className="animated fadeIn">
				<SingleCouponViewAlerts alerts={this.state.alerts} closeAlert={this.closeAlert} />
				<Card>
					<CardBody>
						<SingleCouponViewHeaderPanel coupon={coupon}
							activityStateChangeSuccessHandler={this.handleActivityStateChangeSuccess}
							activityStateChangeFailureHandler={this.openAlert}
							removalSuccessHandler={this.props.removalSuccessHandler}
							removeFailureHandler={(e) => this.openAlert("removeFailure")}
						/>
						<SingleCouponViewGeneralInfo coupon={coupon} />
						<SingleCouponViewScopesTable coupon={coupon}
							addScopeSuccessHandler={addScopeSuccessHandler}
							removeScopesSuccessHandler={removeScopesSuccessHandler}
							toggleScopeSuccessHandler={toggleScopeSuccessHandler}
						/>
					</CardBody>
				</Card>
			</div>
		);
	}
}

export default SingleCouponDetailsView;