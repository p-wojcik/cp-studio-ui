import React, { Component } from 'react';
import {Alert} from 'reactstrap';

class SingleCouponViewAlerts extends Component {

	render() {
		const {alerts, closeAlert} = this.props;

		return (<div>
			<Alert color="success" isOpen={alerts.enableSuccess}
				toggle={(e) => closeAlert("enableSuccess")}>
			  <strong>Coupons has been successfully activated!</strong>
			</Alert>
			<Alert color="danger" isOpen={alerts.enableFailure}
				toggle={(e) => closeAlert("enableFailure")}>
			  <strong>Unexpected error occurred while activating coupon. Please contact administrator.</strong>
			</Alert>
			<Alert color="success" isOpen={alerts.disableSuccess}
				toggle={(e) => closeAlert("disableSuccess")}>
			  <strong>Coupon has been successfully inactivated!</strong>
			</Alert>
			<Alert color="danger" isOpen={alerts.disableFailure}
				toggle={(e) => closeAlert("disableFailure")}>
			  <strong>Unexpected error occurred while inactivating coupon. Please contact administrator.</strong>
			</Alert>
			<Alert color="danger" isOpen={alerts.removeFailure}
				toggle={(e) => closeAlert("removeFailure")}>
			  <strong>Unexpected error occurred while removing coupon. Please contact administrator.</strong>
			</Alert>
		</div>);
	}
}

export default SingleCouponViewAlerts;