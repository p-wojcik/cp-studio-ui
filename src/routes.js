const routes = [
	{
		pattern : '^/$',
		name	: 'Home'
	},
	{
		pattern : '^/dashboard$',
		name	: 'Dashboard'
	},
	{
		pattern : '^/locations$',
		name	: 'Locations'
	},
	{
		pattern : '^/coupons$',
		name	: 'Coupons'
	},
	{
		pattern : '^/coupons/add$',
		name	: 'New coupon'
	},
	{
		pattern : '^/coupons/([0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12})$',
		namePatternMatchIndex : 1
	},
	{
		pattern : '^/subscriptions$',
		name	: 'Subscriptions'
	},


];
export default routes;
