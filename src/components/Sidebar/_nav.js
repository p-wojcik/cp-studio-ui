export default {
  items: [
    {
      name: 'Dashboard',
      url: '/dashboard',
      icon: 'icon-speedometer'
    },
    {
	  name: 'Locations',
	  url: '/locations',
	  icon: 'fa fa-shopping-bag'
	},
    {
	  name: 'Coupons',
	  url: '/coupons',
	  icon: 'fa fa-tags'
	},
	{
	  name: 'Subscriptions',
	  url: '/subscriptions',
	  icon: 'fa fa-feed'
	},
	{
	  name: 'Rating',
	  url: '/rating',
	  icon: 'fa fa-star-half-o'
	},
	{
	  name: 'Payments',
	  url: '/payments',
	  icon: 'fa fa-credit-card-alt'
	},
	{
	  name: 'Settings',
	  url: '/settings',
	  icon: 'fa fa-cog'
	},
	{
	  name: 'Logout',
	  url: '/logout',
	  icon: 'fa fa-power-off'
	}
  ]
};
