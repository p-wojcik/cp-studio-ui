import React, {Component} from 'react';
import {NavLink, Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import {Button, Card, CardBody, CardFooter, Label, Input} from 'reactstrap';
import classNames from 'classnames';
import {mapToCssModules} from 'reactstrap/lib/utils';
import CouponScopesModal from '../CouponScopesModal/CouponScopesModal';

const axios = require('axios');
const GLOBAL_CONFIG = require('../../../config.json');

const propTypes = {
  icon: PropTypes.string,
  color: PropTypes.string,
  link: PropTypes.string,
  className: PropTypes.string,
  cssModule: PropTypes.object
};

const defaultProps = {
  icon: "fa fa-cogs",
  link: "#"
};

class CouponCard extends Component {

  constructor(props) {
    super(props);
    this.state = {
      scopesWindowOpen: false
    };
    this.toggleScopesWindow = this.toggleScopesWindow.bind(this);
  }

  toggleScopesWindow() {
    this.setState({
      scopesWindowOpen: !this.state.scopesWindowOpen
    });
  }

  onCardToggle(card, successHandler, failureHandler) {
  	const httpMethod = card.active ? 'delete' : 'put';
	const options =
	{
		url		: '/coupons/' + card.id + '/active',
		method	: httpMethod,
		baseURL	: GLOBAL_CONFIG.couponService.baseUrl,
		headers	: GLOBAL_CONFIG.couponService.requiredHeaders
	};

	axios.request(options)
		.then((r) => successHandler(card.id))
		.catch((e) => {
			console.error(e);
			failureHandler(card.active);
		});
  }

  getScopesCount(card) {
  	if (card.scopes) {
  		return card.scopes.length;
  	}
  	return 0;
  }

  render() {
    const {
    	className,
    	cssModule,
    	card,
    	icon,
    	link,
    	cardToggleSuccessHandler,
    	cardToggleFailureHandler,
    	scopeToggleSuccessHandler
    	} = this.props;
    const color = 'primary';
    const padding = {panel: "p-3", icon: "p-3", lead: "mt-2"};

    const panel = {style: "clearfix", color: color, icon: icon, classes: ""};
    panel.classes = mapToCssModules(classNames(className, panel.style, padding.panel), cssModule);

    const lead = {style: "h5 mb-0", clear: 'both', color: color, classes: ""};
    lead.classes = classNames(lead.style, 'text-' + panel.color, padding.lead);

    const blockIcon = function (icon) {
      const classes = classNames(icon, 'bg-' + panel.color, padding.icon, "font-2xl mr-3 float-left");
      return (<i className={ classes }></i>);
    };

    return (
      <Card>
        <CardBody className={panel.classes}>
          { blockIcon(panel.icon) }
          <CouponScopesModal
          	onClick={this.toggleScopesWindow}
          	cardId={card.id}
          	cardName={card.name}
          	scopes={card.scopes}
          	modalOpen={this.state.scopesWindowOpen}
          	toggleSuccessHandler={scopeToggleSuccessHandler}
          />
          <div className={ lead.classes }>{ card.name }</div>
          <br/><br/>
          <div className="text-muted text-uppercase font-weight-bold font-xs">{ card.description }</div>
          <br/>
		  <Button size="sm" color="primary" onClick={this.toggleScopesWindow}>
		  	<span className="fa fa-shopping-bag"></span> {this.getScopesCount(card)}
		  </Button>
          <Label className="float-right switch switch-icon switch-primary">
			<Input type="checkbox" className="switch-input" checked={card.active}
				onChange={(e) => this.onCardToggle(card, cardToggleSuccessHandler, cardToggleFailureHandler)}/>
			<span className="switch-label" data-on={'\uf00c'} data-off={'\uf00d'}></span>
			<span className="switch-handle"></span>
		  </Label>
        </CardBody>
        <CardFooter className="px-3 py-2">
			<a className="font-weight-bold font-xs btn-block text-muted" href={"" +link + "/" +card.id}>
				Details <i className="fa fa-angle-right float-right font-lg"></i>
			</a>
	    </CardFooter>
      </Card>
    )
  }
}

CouponCard.propTypes = propTypes;
CouponCard.defaultProps = defaultProps;

export default CouponCard;