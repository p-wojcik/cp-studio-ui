import React, {Component} from 'react';
import {Alert, Input, Label, Modal, ModalHeader, ModalBody, Table} from 'reactstrap';

const GLOBAL_CONFIG = require('../../../config.json');
const axios = require('axios');
const _ = require('lodash/core');

class CouponScopesModal extends Component {

	constructor(props) {
		super(props);
		this.state = {
			errorEnableAlertOpen : false,
			errorDisableAlertOpen : false
		};
		this.displayEnableAlert = this.displayEnableAlert.bind(this);
		this.displayDisableAlert = this.displayDisableAlert.bind(this);
	}

	displayEnableAlert(isVisible) {
		this.setState({
			errorEnableAlertOpen : isVisible
		});
	}

	displayDisableAlert(isVisible) {
		this.setState({
			errorDisableAlertOpen : isVisible
		});
	}

	onScopeToggle(cardId, scope, successHandler) {
      	const httpMethod = scope.active ? 'delete' : 'put';
		const options =
		{
			url		: '/coupons/' + cardId + '/active/location/' + scope.locationId,
			method	: httpMethod,
			baseURL	: GLOBAL_CONFIG.couponService.baseUrl,
			headers	: GLOBAL_CONFIG.couponService.requiredHeaders
		};

		axios.request(options)
			.then((r) => successHandler(cardId, scope.locationId))
			.catch((e) => {
				console.error(e);
				if (scope.active) {
					this.displayDisableAlert(true);
				} else {
					this.displayEnableAlert(true);
				}
			});
    }

    prepareTable(scopes, cardId, toggleSuccessHandler) {
    	if (_.isEmpty(scopes)) {
    		return (<div className="h2 text-center">No scopes available</div>);
    	}

    	let rows = [];
		for (let i=0;i<scopes.length;i++) {
			const scope = scopes[i];
			rows.push(
				<tr key={'scopesrow_'+i}>
					<td>{scope.locationId}</td>
					<td>
						<Label className="switch switch-icon switch-primary">
							<Input type="checkbox" className="switch-input" checked={scope.active}
								onChange={(e) => this.onScopeToggle(cardId, scope, toggleSuccessHandler)}/>
							<span className="switch-label" data-on={'\uf00c'} data-off={'\uf00d'}></span>
							<span className="switch-handle"></span>
						</Label>
					</td>
				</tr>
			);
		}

		return (
			<div>
				<Alert color="warning">
					Remember that any changes to scope's status will not be relevant if coupon is disabled globally.
			    </Alert>
				<Table responsive striped>
					 <thead>
						<tr>
							<th>Location</th>
							<th>Status</th>
						</tr>
					 </thead>
					 <tbody>
						{rows}
					 </tbody>
				 </Table>
			</div>
		);
    }

	render() {
		const {cardId, cardName, scopes, onClick, modalOpen, toggleSuccessHandler} = this.props;
		const table = this.prepareTable(scopes, cardId, toggleSuccessHandler);

		return (
		   <Modal isOpen={modalOpen} toggle={onClick} className="modal-primary">
			 <ModalHeader toggle={onClick}>{cardName}</ModalHeader>
			 <ModalBody>
			   <Alert color="danger" isOpen={this.state.errorEnableAlertOpen} toggle={(e) => this.displayEnableAlert(false)}>
				  <strong>Unexpected error occurred while enabling coupon for given location. Please contact administrator.</strong>
			   </Alert>
			   <Alert color="danger" isOpen={this.state.errorDisableAlertOpen} toggle={(e) => this.displayDisableAlert(false)}>
				  <strong>Unexpected error occurred while disabling coupon for given location. Please contact administrator.</strong>
			   </Alert>
			   {table}
			 </ModalBody>
		   </Modal>
		);
	}
}

export default CouponScopesModal;