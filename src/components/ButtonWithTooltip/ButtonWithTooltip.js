import React from 'react';
import {Button, Tooltip} from 'reactstrap';

class ButtonWithTooltip extends React.Component {

  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.state = {
      tooltipOpen: false
    };
  }

  toggle() {
    this.setState({
      tooltipOpen: !this.state.tooltipOpen
    });
  }

  render() {
  	const {color, cssClass, icon, tooltipText, id, disabled, onClick} = this.props;
    return (
    	<span className={cssClass}>
			<Button disabled={disabled} color={color} size="lg" id={id} onClick={onClick}>
				<span className={icon}></span>
			</Button>
			<Tooltip placement={'top'} isOpen={this.state.tooltipOpen} target={id} toggle={this.toggle}>
			  {tooltipText}
			</Tooltip>
		</span>
    );
  }
}
ButtonWithTooltip.defaultProps = {
	float : 'none'
};

export default ButtonWithTooltip;