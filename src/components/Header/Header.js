import React, {Component} from 'react';
import {
  NavbarToggler,
  NavbarBrand,
} from 'reactstrap';

class Header extends Component {

  mobileSidebarToggle(e) {
    e.preventDefault();
    document.body.classList.toggle('sidebar-mobile-show');
  }

  render() {
    return (
      <header className="app-header navbar">
        <NavbarToggler className="d-lg-none" onClick={this.mobileSidebarToggle}>
          <span className="navbar-toggler-icon"></span>
        </NavbarToggler>
        <NavbarBrand href="#"></NavbarBrand>
      </header>
    );
  }
}

export default Header;
